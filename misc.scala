// Implicits

case class City(code: String, population: Int)
val cities = Map[String,City]("Mumbai" -> City("Mumbai",478447), 
                         "Delhi" -> City("Delhi",11007835),
                         "Pune" -> City("Pune",3115431))

implicit def strToCity(code: String):City = { 
    cities.getOrElse(code,City(code,-1)) 
}

import scala.collection.mutable.ListBuffer
import scala.collection.immutable.List

val visits = ListBuffer[City]()

visits += "Mumbai"
visits += "Pune"

println(visits.toList)

class Fruit {
    override def toString = "Fruit"
}
class Apple extends Fruit {
    override def toString = "Apple"
}
class Orange extends Fruit {
    override def toString = "Orange"
}

class Dog {}

val fruit = new Fruit()
val apple = new Apple()
val orange = new Orange()

def addFruits[T <: Fruit](fruit1: T, fruit2: T, fruit3: T) = List[T](fruit1, fruit2, fruit3)
val lf = addFruits(fruit, apple,orange)
println(lf)

// will not compile
// val lf2 = addFruits(fruit, apple,new Dog())



