def factorial(n: Int): Int = {
    n match {
        case 0 => 1;
        case _ if n > 0 => n * factorial(n-1)
    }
}

println(factorial(5))

// You can also match on case classes

case class Trade(tickerSymbol: String, price: BigDecimal, quantity: BigDecimal) 

def prioritise(t: Trade) = {
    t match {
        case Trade("AAA",_,_) => 1
        case Trade(_,price,quantity) if (price * quantity) > 5000 => 2
        case _ => 3 
    }
}
val trade1 = Trade("AAA",123.45, 35.73)
val trade2 = Trade("BBB",543.21, 12.79)
val trade3 = Trade("CCC",12.21, 45.45)

println(prioritise(trade1))
println(prioritise(trade2))
println(prioritise(trade3))
