// Think Optional when you see Option

// Optional values are the bane of every java developer
// and result in the fondly named NPEs.

// When you think of option imagine a list of 0 or 1 elements

// That makes all the collection methods available on options

// ** safely **

// when there are no nulls, there can be no NullPointerExceptions

val nonEmpty: Option[String] = Some("foo")
val nonEmpty2: Option[String] = Some("bar")
val empty: Option[String] = None

// performing an operation only if value exists

empty foreach {println _}
nonEmpty foreach { println _ }

// extract individual characters

println(nonEmpty.flatten)
println(empty.flatten)

// call a function which itself returns an option

// lets do a lookup from a dictionary

val dict = Map[String,String]("foo" -> "Fooooo", "baz" -> "Baaaaz")

// Lets first do it the non option way
println(dict.get("foo"))
println(dict.get("bar"))

// even if the input was not an option, the output was an option
println(nonEmpty.flatMap(dict.get(_)))
println(nonEmpty2.flatMap(dict.get(_)))
println(empty.flatMap(dict.get(_)))

// what if we wanted to compute the length of the string?

println(nonEmpty.map(_.length))
println(empty.map(_.length))

// test if the option has a value or is a none

println(nonEmpty.isDefined)
println(empty.isDefined)

// note: converse is isEmpty

// Other methods such as filter, exists, forall can be
// performed safely

// Convert a None into an option with a defined default value

println(nonEmpty.orElse(Some("unspecified")))
println(empty.orElse(Some("unspecified")))

// Extract the value or use a default if no value exists
println(nonEmpty.getOrElse("unspecified"))
println(empty.getOrElse("unspecified"))

// Create a list with one or zero items
println(nonEmpty.toList)
println(empty.toList)

// There is also a way to explicitly use pattern matching
// (we'll look at pattern matching in detail later)

val matched = nonEmpty match {
    case Some(x) => x
    case None => "unspecified"
}

println(matched)
