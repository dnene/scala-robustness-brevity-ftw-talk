// classes in principle are fairly similar to those in java eg.

// class Person1 {
//    val firstName: String
//    val lastName: String
//}

//  val p = new Person1("D", "Nene") - will complain too many arguments
//  val p = new Person1() - will complain first name, last name not defined
//  val p = new Person1 {
//      firstName = "D"
//      lastName = "Nene"
//  } - will complain error: reassignment to val
//  In fact even standalone the compiler will complain about the above 
//     - needs to be abstract


class Person2 (val firstName: String,val lastName: String) { 
    override def toString = "Person(" + firstName + "," + lastName + ")"
    // note the lack of parenthesis after greet
    def greet = println("Hello " + firstName)
}

val p = new Person2("D", "Nene")
println(p)
p.greet

class Invoice1(id: String, amount: BigDecimal) {
    override def toString = "Invoice[" + id + "] for Rs. " + amount
}

val i1 = new Invoice1("INV123", 34.56)
println(i1)

// Lets add some invoice items

case class InvoiceItem (itemCode: String, quantity: BigDecimal, rate: BigDecimal){ }

class Invoice(val id: String, items: List[InvoiceItem]) {
    val amount = (BigDecimal(0) /: items) {(acc,i) => acc + i.quantity * i.rate}
    override def toString = "Invoice[" + id + "] for Rs. " + amount
}

val i2 = new Invoice("INV124", 
                List(InvoiceItem("threadXYZ", 11.22, 34.45),
                     InvoiceItem("clothPQR", 157.91, 24.68)))

println(i2)

// Lets talk interfaces

// Interfaces are implemented via traits
// Multiple inheritance supported with traits, though class inheritance still
// restricted to one. There must be one extends if inheriting (class or traits)
// and multiple "with" (traits only)

trait Named {
    def name(): String
}

class Animal (name: String) extends Named { 
    def name() = name
    override def toString = "Animal(" + name + ")"
}

println(new Animal("butch"))

trait Speaker {
    def speak: String
}

trait Mover {
    def move(x: Double, y: Double)
}

// Notice below :
//   var for attributes whose values can change
//   multiple inheritance
//   object Dog - for all helper statics, factory methods etc.

class Dog(name: String, var x: Double, var y: Double) extends Animal(name) with Speaker with Mover {
    val speak = "Woof!"
    def move(x: Double, y: Double) {
        this.x = this.x + x
        this.y = this.y + y
    }
    override def toString = "Dog(" + name + " at (" + x + "," + y + ")"
}

object Dog {
    def apply(name: String, x: Int, y: Int) = {
        new Dog(name,x,y)
    }
}

val d = Dog("Buster", 3,5)
println(d)
println(d.speak)
d.move(1,-1)
println(d)

