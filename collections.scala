// the most basic datatype is list
val list5 = List(1,2,3,4,5)
println(list5)

// or if you want to print them one at a time

list5 foreach { println(_) }

// to be more explicit

list5 foreach { element => println(element) }

// foreach is used for side effects only, since it does not
// return anything

// alternatively

for (element <- list5) {
    println(element)
}

// what if we need a list of squares of numbers 1 thru 5?

val listOfSquares = list5 map { n => n * n }
println(listOfSquares)

// How about a list of only the odd numbers out of the above?

val listOfOdds = list5 filter { _ % 2 != 0 }
val listOfEvens = list5 filterNot { _ % 2 != 0 }
println(listOfOdds + ":" + listOfEvens)

// And what if we want to add up all the values

val sumOfList = list5.foldLeft(0){_ + _}
println(sumOfList)

// to be more explicit
val sumOfList2 = list5.foldLeft(0){(accumulated,element) => accumulated + element}

// or to be more concise
val sumOfList3 = (0 /: list5){_ + _}

// or if you wanted to go in the other direction (if it mattered)
val sumOfList4 = (list5 :\ 0) { _ + _ }
val sumOfList5 = list5.foldRight(0){(element,accumulated) => accumulated + element}

// A reduce is similar to fold but does not require an initialisation

val concatenatedList = list5.map {_.toString} reduceLeft{(acc,ele) => "" + acc + "," + ele}
println(concatenatedList)

// methods to extract beginning or end of list

println(list5 head)
println(list5 tail)
println(list5 init)
println(list5 last)
println(list5 take 2)
println(list5 drop 2)
println(list5 takeWhile {_ < 3})
println(list5 dropWhile {_ < 3})
println(list5 partition {_ < 3})

// other miscellaneous methods
println(list5 reverse)
println(list5 zip(list5 reverse))
println(list5 size)
println(list5 isEmpty)
println(list5 slice(2,4))

// Are all elements greater than zero
println(list5 forall {_ > 0})
// Is at least one element greater than 4
println(list5 exists {_ > 4 })
// What is the sum, product, min & max values of list
println(list5 sum)
println(list5 product)
println(list5 min)
println(list5 max)

// get a comma separated string representation
println(list5 mkString("MyList(",",",")"))

// append to beginning of list
println(0 :: list5)
// append to end of list or concatenate lists
// (try not to do this *NOT CONSTANT TIME*)

println(list5 ::: List(6))

// We saw map earlier. Sometimes the results of
// such functions can return zero or more results
// or wrap the result in a container. In such cases
// a flat map is useful.

// Lets say we want to extract all the characters
// from a list of words. In such a case the function passed
// to map, returns a list of chars per word

val lwords = List("the","quick","brown","fox")
val charsFirstAttempt = lwords map { _.toList }
println(charsFirstAttempt)

// Well we would like to see that list flattened. There's two ways

println(charsFirstAttempt flatten) // or
println(lwords flatMap {_.toList})
